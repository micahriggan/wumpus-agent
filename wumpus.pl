

dungeon(State, Height, Width) :-
	member(wumpus(X, Y), State),
	X1 is X + 1,
	X2 is X - 1,
	Y1 is Y + 1,
	Y2 is Y -1,
	
	append(State, [stench(X1, Y), stench(X2, Y), stench(X, Y1), stench(X, Y2)], Output),
	remove(wumpus(X, Y), Output, Answer),
	respectBounds(Height, Width, Answer, Final),
	write(Final).
	

respectBounds(Height, Width, [], []).

respectBounds(Height, Width, [stench(X, Y) | R], [stench(X, Y) | Others]) :-
	(X > Width; X < 1; Y > Height; Y < 1),
	respectBounds(Height, Width, R, Others).	
	
	
respectBounds(Height, Width, [stench(X, Y) | R], Others) :-
	respectBounds(Height, Width, R, Others),	
	(X =< Width, X > 0; Y =< Height; Y > 0).
	







test(Plan):-   
    solution([at(outside), wants(gold), wumpus(1/3), pit(3/1), pit(3/3), pit(4/4), shiny(3/3)],
                 Plan,
				[gold, at(outside)]).



solution(State, Plan, Goal):-
	solution(State, Plan, [], Goal).

solution(State, Plan, Plan, Goal):-
	is_subset(Goal, State), nl,
	write_solution(State).


solution(State, Plan, Sofar, Goal):-
	choice(Choice, Preconditions, Delete, Add),
	is_subset(Preconditions, State),
	delete_list(Delete, State, Remainder),
	append(Add, Remainder, NewState),
	write(Choice), nl, 
	solution(NewState, Plan, [Choice | Sofar], Goal).


choice(goInCave,
	[at(outside), wants(gold)],
	[at(outside)],
	[at(1/1)]).

choice(go(X/Y1),
	[at(X/Y), safe(X/Y), safe(X/Y1)],
	[at(X/Y)],
	[at(X/Y1)]).

choice(go(X1/Y),
	[at(X/Y), safe(X/Y), safe(X1/Y)],
	[at(X/Y)],
	[at(X1/Y)]).


choice(findGold(X/Y),
	[shiny(X/Y), safe(X/Y), at(X/Y)],
	[shiny(X/Y), wants(gold)],
	[gold]).

choice(leaveCave,
	[at(1/1), gold],
	[at(1/1)],
	[at(outside)]).

is_subset([H|T], Set):-
    member(H, Set),
    is_subset(T, Set).
is_subset([], _).

% Remove all elements of 1st list from second to create third.

delete_list([H|T], List, Final):-
    remove(H, List, Remainder),
    delete_list(T, Remainder, Final).
delete_list([], List, List).
    
remove(X, [X|T], T).
remove(X, [H|T], [H|R]):-
    remove(X, T, R).

write_solution([]).
write_solution([H|T]):-
        write_solution(T),
        write(H), nl.
                 
append([H|T], L1, [H|L2]):-
    append(T, L1, L2).
append([], L, L).

member(X, [X|_]).
member(X, [_|T]):-
    member(X, T).


	